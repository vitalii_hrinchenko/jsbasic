const wallet = {
    userName: "Vitalii",
    bitcoin: {
        logo: "<img width='16px' height='16px' src='https://s2.coinmarketcap.com/static/img/coins/64x64/1.png' alt='bitcoin'>",
        price: 39071.5,
        coins: 6
    },

    ethereum: {
        logo: "<img width='16px' height='16px' src='https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png' alt='ethereum'>",
        price: 2847.1,
        coins: 17
    },

    stellar: {
        logo: "<img width='16px' height='16px' src='https://s2.coinmarketcap.com/static/img/coins/64x64/512.png' alt='stellar'>",
        price: 0.1832,
        coins: 289
    },

    show: function () {
        document.write("<p>Добрий день, " + wallet.userName + "!<br>")
        document.write("На Вашому рахунку " + wallet.bitcoin.logo + "BTC залишилось " + wallet.bitcoin.coins + " монет.<br>");
        document.write("Якщо ви сьогодні їх продасте, то отримаєте " + wallet.bitcoin.price * wallet.bitcoin.coins + " &#36;</p>");

        document.write("<p>На Вашому рахунку " + wallet.ethereum.logo + "ETH залишилось " + wallet.ethereum.coins + " монет.<br>");
        document.write("Якщо ви сьогодні їх продасте, то отримаєте " + wallet.ethereum.price * wallet.ethereum.coins + " &#36;</p>");

        document.write("<p>На Вашому рахунку " + wallet.stellar.logo + "XLM залишилось " + wallet.stellar.coins + " монет.<br>");
        document.write("Якщо ви сьогодні їх продасте, то отримаєте " + wallet.stellar.price * wallet.stellar.coins + " &#36;</p>");
    }
};
wallet.show();